//
// Created by sasha on 18.11.2022.
//
#include "io.h"
#include "exception.h"
#include <bits/types/FILE.h>
#include <stdio.h>
#include <stdlib.h>

int open_read_file(char *pic, FILE **stream) {
    if (!pic)
        return throw_exception(READ_ERROR);
    *stream = fopen(pic, "rb");
    if (stream == NULL)
        return throw_exception(READ_ERROR);
    else
        return READ_OK;
}

int open_write_file(char *pic, FILE **stream) {
    *stream = fopen(pic, "wb");
    if (stream == NULL) {
        return throw_exception(OPEN_ERROR);
    }
    return OPEN_OK;
}

int close_file(FILE **stream) {
    if (!*stream) {
        return throw_exception(CLOSE_ERROR);
    }
    int checker = fclose(*stream);
    if (checker == EOF) {
        return throw_exception(CLOSE_ERROR);
    }
    return CLOSE_OK;
}
