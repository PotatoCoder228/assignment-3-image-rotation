//
// Created by sasha on 18.11.2022.
//
#include "exception.h"

int throw_exception(int ex) {
    switch (ex) {
        case READ_INVALID_ARGS:
            fprintf(stderr, "Неправильные аргументы.");
            return READ_INVALID_ARGS;
        case READ_ERROR:
            fprintf(stderr,"Ошибка чтения.");
            return READ_ERROR;
        case READ_NOT_BMP:
            fprintf(stderr,"Файл не BMP формата.");
            return READ_NOT_BMP;
        case WRITE_ERROR:
            fprintf(stderr,"Ошибка записи в файл.");
            return WRITE_ERROR;
        case IMAGE_ROTATION_ERROR:
            fprintf(stderr,"Ошибка поворота изображения.");
            return IMAGE_ROTATION_ERROR;
        case OPEN_ERROR:
            fprintf(stderr,"Ошибка при открытии файла.");
            return OPEN_ERROR;
        case CLOSE_ERROR:
            fprintf(stderr,"Ошибка при закрытии файла.");
            return CLOSE_ERROR;
        case READ_FROM_NULL_FILE:
            fprintf(stderr,"Чтение из пустого файла.");
            return READ_FROM_NULL_FILE;
        default:
            fprintf(stderr,"Рандомная ошибОчка");
            return -1;
    }
}
