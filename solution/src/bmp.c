//
// Created by sasha on 18.11.2022.
//

#include "bmp.h"
#include "exception.h"
#include "image.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


uint8_t calculate_padding(size_t image_width) {
    return image_width % 4;
}

size_t padding_size(struct image const *img) {
    return img->height * img->width * calculate_padding(img->width);
}

/*
 * записываем мусорные биты, если необходимо
 * */

int write_padding(FILE *const out, size_t image_width) {
    size_t zero = 0;
    if (!fwrite(&zero, calculate_padding(image_width), 1, out))
        return throw_exception(WRITE_ERROR);
    return WRITE_OK;
}

struct bmp_header create_headers(const struct image *image) {
    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + img_get_size(image) + padding_size(image);
    header.bfReserved = 0;
    header.biSize = 40;
    header.bOffBits = 54;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;//всегда 1
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = 0;//кажется, тут всё же 0
    header.biXPelsPerMeter = 2834;
    header.biYPelsPerMeter = 2834;
    header.biClrUsed = 0;//таблицы цветов нет => 0
    header.biClrImportant = 0; // Просто ставим 0
    return header;
}

bool header_validation(struct bmp_header const *pic_header) {
    return pic_header->biHeight > 0 && pic_header->biWidth > 0 && pic_header->bfType == 0x4D42;
}

/*
 * Считываем размер и выделяем память
 * Далее, читаем в наш массив данных пиксели в 1 линию
 * */
static int read_pixels(FILE *file, struct bmp_header const *headers, struct image *img) {
    struct pixel pixel = {0};
    img->width = headers->biWidth;
    img->height = headers->biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) return throw_exception(READ_INVALID_BITS);
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            if (fread(&pixel, sizeof(struct pixel), 1, file) != 1) {
                img_free_memory(img);
                return throw_exception(READ_INVALID_BITS);
            }
            img_set_pixel(img, pixel, j, i);
        }
        fseek(file, calculate_padding(img->width), SEEK_CUR);
    }
    return READ_OK;
}

/*
 * Записываем массив пикселей в наш файл
 * */

static int write_pixels(FILE *file, struct image const *img) {
    struct pixel pixel;
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            pixel = img_get_pixel(img, j, i);
            if (fwrite(&pixel, sizeof(struct pixel), 1, file) != 1)
                return throw_exception(WRITE_ERROR);
        }
        if (write_padding(file, img->width) == WRITE_ERROR)
            return WRITE_ERROR;
    }
    return WRITE_OK;
}


int from_bmp(FILE *file, struct image *img) {
    struct bmp_header pic_header = {0};
    if (!file) return throw_exception(READ_FROM_NULL_FILE);
    if (fread(&pic_header, sizeof(struct bmp_header), 1, file) != 1)
        return throw_exception(READ_INVALID_BITS);
    if (!header_validation(&pic_header)) return throw_exception(READ_INVALID_HEADER);
    return read_pixels(file, &pic_header, img);
}

int to_bmp(FILE *file, struct image const *img) {
    struct bmp_header pic_header;
    if (!file || !img) return throw_exception(WRITE_ERROR);
    pic_header = create_headers(img);
    if (fwrite(&pic_header, sizeof(struct bmp_header), 1, file) != 1)
        return throw_exception(WRITE_ERROR);
    return write_pixels(file, img);
}
