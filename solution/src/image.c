//
// Created by sasha on 18.11.2022.
//

#include "image.h"


/*
 * Инициализация картинки в виде структуры
 * */

struct image image(size_t width, size_t height) {
    struct image image = {0};
    image.data = malloc(sizeof(struct pixel) * width * height);
    image.height = height;
    image.width = width;
    return image;
}

/*
 * Геттеры и сеттеры
 * */

void img_set_pixel(struct image *image, struct pixel const pixel, size_t x, size_t y) {
    *(image->data + y * image->width + x) = pixel;
    /*отмерили шаг, на который нам нужно прыгнуть в одномерном массиве
     * последовательно идущих друг за другом строчек картинки
     * и задали в нужном месте нужный пиксель
     * */
}

struct pixel img_get_pixel(struct image const *image, size_t x, size_t y) {
    return *(image->data + y * image->width + x);
}


/*
 * Получение размера изображения в байтах
 * */

size_t img_get_size(struct image const *img) {
    return img->width * img->height * sizeof(struct pixel);
}

/*
 * Освобождение памяти
 * */

void img_free_memory(struct image *image) {
    free(image->data);
}
