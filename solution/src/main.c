#include "bmp.h"
#include "exception.h"
#include "image.h"
#include "io.h"
#include "transformer.h"
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

int main(int argc, char **argv) {
    struct image input_image = {0};
    struct image output_image = {0};

    FILE *input_stream = NULL;
    FILE *output_stream = NULL;

    if (argc != 3) {
        throw_exception(READ_INVALID_ARGS);
        return EXIT_FAILURE;
    } else {


        open_read_file(argv[1], &input_stream);

        if (from_bmp(input_stream, &input_image) != READ_OK) {
            close_file(&input_stream);
            close_file(&output_stream);
            return EXIT_FAILURE;
        }

        output_image = rotate_on_90_degrees(input_image);

        open_write_file(argv[2], &output_stream);

        if (!output_image.data) {
            close_file(&input_stream);
            close_file(&output_stream);
            img_free_memory(&input_image);
            throw_exception(IMAGE_ROTATION_ERROR);
            return EXIT_FAILURE;
        }

        img_free_memory(&input_image);

        if (!to_bmp(output_stream, &output_image)) {
            close_file(&input_stream);
            close_file(&output_stream);
            img_free_memory(&output_image);
            return EXIT_FAILURE;
        }

        img_free_memory(&output_image);
        close_file(&input_stream);
        close_file(&output_stream);
        printf("Преобразованный файл готов.\n");
        return 0;
    }
}
