//
// Created by sasha on 18.11.2022.
//
#include "image.h"
#include "transformer.h"

/*
 * Формируем структуру с новым изображением
 * */
struct image rotate_on_90_degrees(struct image const img) {
    struct image rotated_image = image(img.height, img.width);

    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j++) {
            /*
             * Делаемм так, т.к. в BMP у нас идут пиксели в обратном порядке
             * */
            img_set_pixel(&rotated_image, img_get_pixel(&img, j, img.height - i - 1), i, j);
        }
    }
    return rotated_image;
}
