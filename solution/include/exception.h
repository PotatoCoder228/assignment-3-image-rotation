//
// Created by sasha on 18.11.2022.
//

#ifndef IMAGE_TRANSFORMER_EXCEPTION_H
#define IMAGE_TRANSFORMER_EXCEPTION_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum read_status {
    READ_OK = 0,
    READ_ERROR,
    READ_INVALID_ARGS,
    READ_NOT_BMP,
    READ_FROM_NULL_FILE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = READ_INVALID_HEADER + 1,
    WRITE_ERROR
};

enum open_close_stat {
    OPEN_OK = WRITE_ERROR + 1,
    OPEN_ERROR,
    CLOSE_OK,
    CLOSE_ERROR
};

enum rotate_stat {
    IMAGE_ROTATION_ERROR = CLOSE_ERROR + 1,
};

int throw_exception(int ex);

#endif //IMAGE_TRANSFORMER_EXCEPTION_H
