//
// Created by sasha on 18.11.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image {
    size_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t r, g, b;
};

struct image image(size_t width, size_t height);

struct pixel img_get_pixel(struct image const *image, size_t x, size_t y);

size_t img_get_size(struct image const *img);

void img_free_memory(struct image *image);

void img_set_pixel(struct image *image, struct pixel const pixel, size_t x, size_t y);

#endif //IMAGE_TRANSFORMER_IMAGE_H
