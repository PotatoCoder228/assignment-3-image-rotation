//
// Created by sasha on 18.11.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include <stdbool.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

uint8_t calculate_padding(size_t image_width);

size_t padding_size(struct image const *img);

int write_padding(FILE *const out, size_t image_width);


struct bmp_header create_headers(const struct image *image);

bool header_validation(struct bmp_header const *header);

int from_bmp(FILE *file, struct image *img);

int to_bmp(FILE *file, struct image const *img);

#endif //IMAGE_TRANSFORMER_BMP_H
