//
// Created by sasha on 18.11.2022.
//

#ifndef IMAGE_TRANSFORMER_TRANSFORMER_H
#define IMAGE_TRANSFORMER_TRANSFORMER_H

struct image rotate_on_90_degrees(struct image const img);

#endif //IMAGE_TRANSFORMER_TRANSFORMER_H
