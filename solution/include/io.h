//
// Created by sasha on 18.11.2022.
//

#ifndef IMAGE_TRANSFORMER_IO_H
#define IMAGE_TRANSFORMER_IO_H

#include <bits/types/FILE.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int open_read_file(char *pic, FILE **stream);

int close_file(FILE **stream);

int open_write_file(char *pic, FILE **stream);

#endif //IMAGE_TRANSFORMER_IO_H
